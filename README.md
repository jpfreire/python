# Python

Bloco de notas do python

## Instalando várias versões do python

Instalação do [pyenv][1]  para gergenciar as várias versões

[1]: https://github.com/pyenv/pyenv/ "Pyenv - versão 1.2.16 instalada"


```bash
brew update
brew install pyenv
brew upgrade pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.zshrc

# Instala uma versão do python
pyenv install 3.8.1

# Define a versão global do python
pyenv global 3.8.1

# Define a versão do python no diretório atual
pyenv local 3.8.1

```

ref.: https://www.ianmaddaus.com/post/manage-multiple-versions-python-mac/

## Poetry

Gerência de virtual environments e pacotes
```bash
poetry init
poetry add selenium   # adiciona o selenium como dependência do projeto
poetry install        # instala as dependências
poetry shell          # ativa o ambiente virtual 
code .
```

## Dynaconf 

Configuração de variáveis de ambiente

```bash
poetry add dynaconf
export ENV_FOR_DYNACONF=development
```
```
.secrets.toml
    [default]
    password = "Utopi@"
```
```python
from dynaconf import settings
conn = Client(
    username=settings.USERNAME,             # attribute style access
    password=settings.get('PASSWORD'),      # dict get style access
    port=settings['PORT'],                  # dict item style access
    timeout=settings.as_int('TIMEOUT'),     # Forcing casting if needed
    host=settings.get('HOST', 'localhost')  # Providing defaults
)
```
